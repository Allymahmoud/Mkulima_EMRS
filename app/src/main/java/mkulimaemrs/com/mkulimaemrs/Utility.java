package mkulimaemrs.com.mkulimaemrs;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Calendar;
import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;


/**
 * Created by Allymahmoud on 10/12/17.
 */

public class Utility {
    private String confirmedPassword = "";
    public static String gameMode = "hard";
    public static Client newClient;
    public static int no_familyMembers = 0;
    public static HospitalVisit hospitalVisit;



    public static boolean vibrationMode = true;
    public static boolean soundMode = true;

    public Utility(){
    }


    public boolean isVibrationMode() {
        return vibrationMode;
    }

    public  void setVibrationMode(boolean vibrationMode) {
        Utility.vibrationMode = vibrationMode;
    }

    public boolean isSoundMode() {
        return soundMode;
    }

    public void setSoundMode(boolean soundMode) {
        Utility.soundMode = soundMode;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String newGameMode) {
        gameMode = newGameMode;
    }

    public HospitalVisit getHospitalVisit() {
        return hospitalVisit;
    }

    public void setHospitalVisit(HospitalVisit hospitalVisitNew) {
        hospitalVisit = hospitalVisitNew;
    }

    /**
     * method to make Toast
     */
    public void makeToast(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Code to check for runtime permissions.
     */

    public void checkPermissions(Activity activity) {
        if(Build.VERSION.SDK_INT < 23)
            return;

        if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || activity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }


        final int MY_PERMISSIONS_REQUEST = 301;

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity,Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(activity,Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(activity,Manifest.permission.INTERNET)
                        != PackageManager.PERMISSION_GRANTED){


            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET},
                    MY_PERMISSIONS_REQUEST);

        }
    }

    public void segueToNewActivity(Activity activity1, Class activity2){
        Intent segueIntent = new Intent(activity1, activity2);
        activity1.startActivity(segueIntent);

    }
    public void segueToNewActivity(Activity activity1, Class activity2, String key, String value){
        Intent segueIntent = new Intent(activity1, activity2).putExtra(key, value);
        activity1.startActivity(segueIntent);

    }
    public void segueToNewActivityAndClearPrev(Activity activity1, Class activity2){
        Intent intent = new Intent(activity1.getApplicationContext(), activity2);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity1.startActivity(intent);
    }



        //function to show the alertView
    public void alertView( String title, String message, Context activityContext) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activityContext);

        dialog.setTitle( title )
                .setIcon(R.drawable.defaultphoto)
                .setMessage(message)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                    }})
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }


    public Client getUserInfo(DatabaseReference mDatabase, String userId){

        Query userInfoQuery = mDatabase.child("clients").child(userId);
        userInfoQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                newClient = dataSnapshot.getValue(Client.class);
                if (newClient != null){

                    Log.d("Utility ","client email is" + newClient.email);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return newClient;


    }






    public void clearUserInfoFromSharedPrefernce(Activity activity, String SHARED_PREF){
        SharedPreferences sp = activity.getSharedPreferences(SHARED_PREF, 0);

        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();

    }

    public void saveLoginInfoToSharedPreference(Activity activity, final TextView Email, final TextView Password, final String SHARED_PREF){
        SharedPreferences sp = activity.getSharedPreferences(SHARED_PREF, 0);

        SharedPreferences.Editor editor = sp.edit();

        editor.putString("email", Email.getText().toString());
        editor.commit();

        SharedPreferences.Editor editor1 = sp.edit();
        editor1.putString("password", Password.getText().toString());
        editor1.commit();
    }

    public String timeStampMedium(){
        Calendar calendar = Calendar.getInstance();
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

        return dateFormat.format(calendar.getTime());
    }
    public String timeStampFull(){
        Calendar calendar = Calendar.getInstance();
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL);

        return dateFormat.format(calendar.getTime());
    }
    public String[] splitString(String input, String delimeter){
        String[] outputArray = input.split(delimeter);
        return outputArray;
    }
    public boolean isNumeric(String string){
        if (string.matches("\\d+(?:\\.\\d+)?")){
            return true;
        }
        return false;
    }

    public String hashEmailKey(String email){
        email = email.toLowerCase();
        String[] emailKeyArray = email.split("\\.");
        return strJoin(emailKeyArray,"*");
    }
    private static String strJoin(String[] aArr, String sSep) {
        StringBuilder sbStr = new StringBuilder();
        for (int i = 0, il = aArr.length; i < il; i++) {
            if (i > 0)
                sbStr.append(sSep);
            sbStr.append(aArr[i]);
        }
        return sbStr.toString();
    }
    public long computeTimeAfterDays(int day){
        Calendar calendar = Calendar.getInstance();
        if (day > 0){
            return calendar.getTimeInMillis()+(1000*60*60*24*day);
        }else{
            return calendar.getTimeInMillis();
        }

    }

    public String computeDateAfterDays(int day){
        Calendar calendar = Calendar.getInstance();
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        if(day>0){
            long newDate = calendar.getTimeInMillis()+(1000*60*60*24*day);
            return dateFormat.format(newDate);
        }else{
            return dateFormat.format(calendar.getTimeInMillis());
        }

    }

    public String capitalizeEveryWordInSentence(String str){
        String[] strArray = str.split(" ");
        StringBuilder builder = new StringBuilder();
        for (String s : strArray) {
            String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
            builder.append(cap + " ");
        }
        return builder.toString();
    }



}
