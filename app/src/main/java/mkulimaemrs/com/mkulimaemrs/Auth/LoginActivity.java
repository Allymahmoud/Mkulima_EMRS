package mkulimaemrs.com.mkulimaemrs.Auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mkulimaemrs.com.mkulimaemrs.MainActivity;
import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;

public class LoginActivity extends AppCompatActivity {
    private Utility utility;
    private EditText email;
    private EditText password;
    private ProgressDialog progressDialog;
    private DatabaseReference mDatabase;
    public static String SHARED_PREF = "Login_Info";
    SharedPreferences sharedPreferences;

    private FirebaseAuth firebaseAuth;

    private Client currentClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = (EditText) findViewById(R.id.input_email);
        password = (EditText) findViewById(R.id.input_password);
        utility = new Utility();
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        progressDialog = new ProgressDialog(this);

        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
        }

    }

    public void login_action(View view) {
        loginUser();

    }

    private void IsEmailVerified() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user !=null && user.isEmailVerified()) {
            finish();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            utility.alertView("Error!","Please verify your email",LoginActivity.this);
        }

    }

    public void signUp_action(View view) {
        utility.segueToNewActivity(this,SignUpActivity.class);
    }

//    public void forgotPassword_action(View view) {
//        utility.segueToNewActivity(this,ResetPasswordActivity.class);
//    }

    private void loginUser(){

        final String input_email = email.getText().toString().trim();
        final String input_password = password.getText().toString().trim();

        if (TextUtils.isEmpty(input_email) || TextUtils.isEmpty(input_password)){
            utility.alertView("Could not Login", "no field can be empty", this);
        }else{
            //save the info to shared preferences
            utility.saveLoginInfoToSharedPreference(this, email,password,SHARED_PREF);

            progressDialog.setMessage("Loging user... ");
            progressDialog.show();

            firebaseAuth.signInWithEmailAndPassword(input_email,input_password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                progressDialog.cancel();
                                IsEmailVerified();



                                //utility.segueToNewActivityAndClearPrev(LoginActivity.this,MainActivity.class);
//                                utility.segueToNewActivity(LoginActivity.this,MainActivity.class);



                            }else{
                                progressDialog.cancel();
                                utility.makeToast(LoginActivity.this, task.getException().getLocalizedMessage());
                                utility.alertView("Error!", task.getException().getLocalizedMessage(),LoginActivity.this);

                            }

                        }
                    });


        }
    }




}
