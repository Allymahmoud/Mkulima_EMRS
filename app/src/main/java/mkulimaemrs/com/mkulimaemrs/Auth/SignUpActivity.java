package mkulimaemrs.com.mkulimaemrs.Auth;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;

public class SignUpActivity extends AppCompatActivity {
    private Utility utility;
    private EditText email;
    private EditText name;
    private EditText password;
    private EditText confirmPassword;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;
    private Context context;
    private Activity activity;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        progressDialog = new ProgressDialog(this);
        email = (EditText) findViewById(R.id.input_email);
        name = (EditText) findViewById(R.id.input_name);
        password = (EditText) findViewById(R.id.input_password);
        confirmPassword = (EditText) findViewById(R.id.input_confirmpassword);
        firebaseAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        context = this;
        activity = this;




        utility = new Utility();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        finish();
        utility.segueToNewActivity(SignUpActivity.this,LandingActivity.class);

    }

    public void signUp_action(View view) {
        registerUser();

        //this.finish();
    }

    private void registerUserInfo(Client newClient){
        final FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(newClient.name).build();


        if (mFirebaseUser != null){
            mFirebaseUser.updateProfile(profileUpdates);
            String uid = mFirebaseUser.getUid();
            mDatabase.child("clients").child(uid).setValue(newClient)
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            progressDialog.cancel();
                            if (task.isSuccessful()){
                                mFirebaseUser.sendEmailVerification();
                                finish();
                                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                //utility.segueToNewActivity(activity,LoginActivity.class);


                            }else {
                                Log.d("task failure",task.getException().getMessage());
                                utility.alertView("Error!, Could Not Register User", task.getException().getLocalizedMessage(),context);

                            }

                        }
                    });



        }else{
            utility.alertView("Could Not Register User","Something went wrong", this);
        }
    }


    private void registerPatient(String name, String email, String password, String role){
        FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();
        if (mFirebaseUser != null){
            String userId = mFirebaseUser.getUid();
            final Client newClient = new Client(name,email,password,role,userId);

            registerUserInfo(newClient);
        }


    }



    private void proceedWithUserRegistration(final String input_name, final String input_email,  final String input_password, final String input_role){
        progressDialog.setMessage("Registering user to the server");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(input_email, input_password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            registerPatient(input_name, input_email, input_password, input_role);

                        }else {
                            progressDialog.cancel();
                            Log.d("task failure",task.getException().getMessage());

                            utility.alertView("Error!", task.getException().getLocalizedMessage(),context);

                        }


                    }
                });

    }

    private void registerUser(){
        final String input_name = name.getText().toString().trim();
        final String input_email = email.getText().toString().trim();
        final String input_password = password.getText().toString().trim();
        final String input_confirm_password = confirmPassword.getText().toString().trim();



        if (TextUtils.isEmpty(input_email) || TextUtils.isEmpty(input_password) || TextUtils.isEmpty(input_confirm_password) || TextUtils.isEmpty(input_name)){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else if (!input_password.equalsIgnoreCase(input_confirm_password) ){
            utility.alertView("Could not proceed", "password must much", this);
        }
        else {
            proceedWithUserRegistration(input_name, input_email, input_password, "patient");
        }
    }







}
