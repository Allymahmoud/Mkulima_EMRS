package mkulimaemrs.com.mkulimaemrs.Auth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;

public class LandingActivity extends AppCompatActivity {

    private Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landpage);
        utility = new Utility();
    }

    public void signin_action(View view) {
        finish();
        utility.segueToNewActivity(this,LoginActivity.class);
    }

    public void signUp_action(View view) {
        finish();
        utility.segueToNewActivity(this,SignUpActivity.class);
    }
}
