package mkulimaemrs.com.mkulimaemrs.health;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;

/**
 * Created by Allymahmoud on 11/19/17.
 */

public class RequestListViewAdaptor extends BaseAdapter {
    HospitalVisit[] hospitalVisits;
    Context context;
    Activity activity;
    private static LayoutInflater inflater=null;
    private ProgressDialog imageProgressDialog;
    private Utility utility;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    public RequestListViewAdaptor(HospitalVisit [] hospitalVisits, Activity activity){
        this.hospitalVisits = hospitalVisits;
        context = activity;
        this.activity = activity;
        utility = new Utility();
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return hospitalVisits.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public HospitalVisit[] getHospitalVisits(){
        return hospitalVisits;
    }
    public void setHospitalVisits(HospitalVisit[] hospitalVisits){
        this.hospitalVisits = hospitalVisits;

    }


    public class Holder
    {
        TextView schedulee_name;
        TextView pickudate;
        ImageView icon;

    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.row_item, null);
        holder.schedulee_name=(TextView) rowView.findViewById(R.id.name);
        holder.pickudate = (TextView) rowView.findViewById(R.id.description);
        holder.icon = (ImageView) rowView.findViewById(R.id.pettedIcon);

        holder.schedulee_name.setText(hospitalVisits[i].date);
        holder.pickudate.setText(hospitalVisits[i].hospitalName);
        //holder.serviceImage.setImageResource(missedPickupReports[i].imageResouce);


        /*
        DownloadImage newDownloadThread = new DownloadImage(holder.catImage);
        newDownloadThread.execute(missedPickupReports[i].getPicUrl());
        myDownloadThreads.add(newDownloadThread);
        */

        final int position = i;
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You Clicked "+ hospitalVisits[position].hospitalName, Toast.LENGTH_SHORT).show();
                utility.setHospitalVisit(hospitalVisits[position]);
                utility.segueToNewActivity(activity,HospitalVisitDetailActivity.class);

            }
        });

        return rowView;
    }


}
