package mkulimaemrs.com.mkulimaemrs.health;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;

public class MedicalRecordsActivity extends AppCompatActivity {

    ListView requestListView;
    Utility utility = new Utility();
    HospitalVisit[] hospitalVisits;
    RequestListViewAdaptor requestListViewAdaptor;
    FirebaseAuth firebaseAuth;
    DatabaseReference mDatabase;
    Client currentClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_records);

        requestListView = (ListView)findViewById(R.id.request_list);
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getHospitalVistList();
    }
//    private void getNewListFromDatabase(){
//
//        if (firebaseAuth.getUid() != null) {
//            String userId = firebaseAuth.getUid();
//
//            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("PickupScheduled");
//            myRef.addChildEventListener(new ChildEventListener() {
//                @Override
//                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                    getHospitalVistList();
//                }
//
//                @Override
//                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                    getHospitalVistList();
//                }
//
//                @Override
//                public void onChildRemoved(DataSnapshot dataSnapshot) {
//                    getHospitalVistList();
//
//                }
//
//                @Override
//                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                    getHospitalVistList();
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//                    getHospitalVistList();
//
//
//                }
//            });
//            //myRef.addListenerForSingleValueEvent();
//        }
//    }

    private  void getHospitalVistList(){
        Query schedulePickUpDQuery = mDatabase.child("clients").child(currentClient.id).child("hospitalVisits");
        schedulePickUpDQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = (int)dataSnapshot.getChildrenCount();
                if (size>0){
                    int sizeArray=0;

                    for (DataSnapshot child : dataSnapshot.getChildren()){
                        HospitalVisit scheduledPickup = child.getValue(HospitalVisit.class);

                        sizeArray++;

                    }

                    if (sizeArray > 0) {
                        hospitalVisits = new HospitalVisit[sizeArray];
                        int i = 0;
                        for (DataSnapshot child : dataSnapshot.getChildren()){
                            HospitalVisit scheduledPickup = child.getValue(HospitalVisit.class);

                            hospitalVisits[i]=scheduledPickup;
                                i++;


                        }
                        if (requestListViewAdaptor != null){
                            requestListViewAdaptor.setHospitalVisits(hospitalVisits);
                            requestListViewAdaptor.notifyDataSetChanged();

                        }else {
                            requestListViewAdaptor = new RequestListViewAdaptor(hospitalVisits, MedicalRecordsActivity.this);
                            requestListView.setAdapter(requestListViewAdaptor);

                        }


                    }



                }
                Log.d("snapshot value",String.valueOf(dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
