package mkulimaemrs.com.mkulimaemrs.health;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;

public class HospitalVisitDetailActivity extends AppCompatActivity {

    private TextView hospitalName;
    private  TextView doctorName;
    private TextView symptoms;
    private TextView diagnosis;
    private TextView medication;
    private  TextView diagnosisResults;
    private  TextView date;
    private ImageView profileImage;
    private Utility utility;
    private HospitalVisit hospitalVisit;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_visit_detail);

        hospitalName = (TextView) findViewById(R.id.hospital_name);
        doctorName = (TextView) findViewById(R.id.doctors_name);
        symptoms = (TextView) findViewById(R.id.symptoms);
        diagnosis = (TextView) findViewById(R.id.diagnosis);
        medication = (TextView) findViewById(R.id.medication);
        diagnosisResults = (TextView) findViewById(R.id.results);
        date = (TextView) findViewById(R.id.date);

        profileImage = (ImageView)findViewById(R.id.client_image);
        utility = new Utility();
        hospitalVisit = utility.getHospitalVisit();

        configUI();


    }


    private void configUI(){
        if (hospitalVisit != null){
            hospitalName.setText(hospitalVisit.hospitalName);
            doctorName.setText(hospitalVisit.doctorName);
            symptoms.setText(hospitalVisit.symptoms);
            diagnosis.setText(hospitalVisit.diagnosis);
            medication.setText(hospitalVisit.medication);
            diagnosisResults.setText(hospitalVisit.results);
            date.setText(hospitalVisit.date);

            if (hospitalVisit.imageUrl.length() > 5 ){
                Glide.with(this).load(hospitalVisit.imageUrl).into(profileImage);
            }


        }
        else{
            Log.d("found hospital visit", "nulllll");
            hospitalVisit = utility.getHospitalVisit();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void enlargeImage_action(View view) {

    }
}
