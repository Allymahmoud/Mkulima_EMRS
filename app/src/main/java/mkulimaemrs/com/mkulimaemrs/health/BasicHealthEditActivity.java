package mkulimaemrs.com.mkulimaemrs.health;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.MedicalRecord;

public class BasicHealthEditActivity extends AppCompatActivity {
    private Utility utility;

    private TextView medicalRecordCount;
    private TextView medicalRecordViewers;
    private EditText height;
    private  EditText weight;
    private TextView name;
    private EditText bloodGroup;
    private  EditText BMI;
    private ImageView profileImage;
    private Client currentClient;


    private RadioButton radioMalebtn;
    private RadioButton radioFemalebtn;


    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app


    private String  sex;

    TextView allergiesSelected;
    String[] listItems;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_health_edit);

        name = (TextView) findViewById(R.id.client_name);
        height = (EditText) findViewById(R.id.height);
        weight = (EditText) findViewById(R.id.weight);
        bloodGroup = (EditText) findViewById(R.id.blood_group);

        radioMalebtn = (RadioButton)findViewById(R.id.male);
        radioFemalebtn = (RadioButton)findViewById(R.id.female);
        medicalRecordCount = (TextView) findViewById(R.id.medical_record_count);
        medicalRecordViewers = (TextView) findViewById(R.id.medical_record_viewers);




        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        utility = new Utility();
        utility.checkPermissions(this);
        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            sex = currentClient.sex;
        }

        allergiesSelected = (TextView) findViewById(R.id.allergies);

        listItems = getResources().getStringArray(R.array.allergy_item);
        checkedItems = new boolean[listItems.length];


        utility = new Utility();
        configUI();

    }




    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void saveAction(View view) {
        // TODO: save the info to firebase
        saveUserMedicalInfo();
    }

    private void saveUserMedicalInfo(){
        String input_height = height.getText().toString().trim();
        String input_weight = weight.getText().toString().trim();
        String input_bloodGroup = bloodGroup.getText().toString().trim();
        String input_allergy = allergiesSelected.getText().toString().trim();

        String input_sex = sex;



        if (TextUtils.isEmpty(input_height) || TextUtils.isEmpty(input_weight) || TextUtils.isEmpty(input_bloodGroup) || input_sex == null || TextUtils.isEmpty(input_allergy)){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else if((!utility.isNumeric(input_height)) || (!utility.isNumeric(input_weight)) ){
            utility.alertView("Could not proceed", "height and weight must be numeric", this);
        }
        else {
            Double height_double = Double.parseDouble(input_height)* 0.01;
            Double weight_double = Double.parseDouble(input_weight);
            Double bmi_double = weight_double/(height_double*height_double);

            String bmi_String = String.format( "%.2f", bmi_double);

            MedicalRecord medicalRecord = new MedicalRecord(input_height, input_weight, bmi_String, input_bloodGroup, input_allergy);

            final FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();

            if (mFirebaseUser != null){
                String uid = mFirebaseUser.getUid();
                mDatabase.child("clients").child(uid).child("sex").setValue(input_sex);
                mDatabase.child("clients").child(uid).child("medicalRecord").setValue(medicalRecord)
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    finish();


                                }else {
                                    Log.d("task failure",task.getException().getMessage());
                                    utility.alertView("Error!, Could Not save medical records", task.getException().getLocalizedMessage(),BasicHealthEditActivity.this);

                                }

                            }
                        });

            }else{
                utility.alertView("Could Not Save changes","failed to retrieve user", this);
            }

        }

    }

    public void addAllergy(View view) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(BasicHealthEditActivity.this);
        mBuilder.setTitle(R.string.dialog_title);
        mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {
//                        if (isChecked) {
//                            if (!mUserItems.contains(position)) {
//                                mUserItems.add(position);
//                            }
//                        } else if (mUserItems.contains(position)) {
//                            mUserItems.remove(position);
//                        }
                if(isChecked){
                    mUserItems.add(position);
                }else{
                    mUserItems.remove((Integer.valueOf(position)));
                }
            }
        });

        mBuilder.setCancelable(false);
        mBuilder.setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                String item = "";
                for (int i = 0; i < mUserItems.size(); i++) {
                    item = item + listItems[mUserItems.get(i)];
                    if (i != mUserItems.size() - 1) {
                        item = item + ", ";
                    }
                }
                allergiesSelected.setText(item);
            }
        });

        mBuilder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.setNeutralButton(R.string.clear_all_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                for (int i = 0; i < checkedItems.length; i++) {
                    checkedItems[i] = false;
                    mUserItems.clear();
                    allergiesSelected.setText("");
                }
            }
        });

        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    public void sexSelected(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.male:
                if (checked)
                    // Pirates are the best
                    sex = "Male";
                utility.makeToast(this,"male");
                break;
            case R.id.female:
                if (checked)
                    // Ninjas rule
                    sex = "Female";
                utility.makeToast(this,"female");
                break;
        }
    }


    private void configUI(){
        if (currentClient != null){
            name.setText(currentClient.name);

            if (currentClient.medicalRecord != null){
                height.setText(currentClient.medicalRecord.height + " cm");
                weight.setText(currentClient.medicalRecord.weight + " Kg");
                bloodGroup.setText(currentClient.medicalRecord.bloodType);

                allergiesSelected.setText(currentClient.medicalRecord.allergies);

            }
            if (currentClient.hospitalVisits != null){
                String noMedicalRecords = String.valueOf(currentClient.hospitalVisits.size());
                medicalRecordCount.setText(noMedicalRecords);
            }
            else{
                medicalRecordCount.setText("0");
            }
            if (currentClient.medicalViewers != null){
                medicalRecordViewers.setText(String.valueOf(currentClient.medicalViewers.size()));
            }else {
                medicalRecordViewers.setText("0");
            }


            // configure sex
            if (!currentClient.sex.isEmpty()){
                if (currentClient.sex.contentEquals("Female")){
                    radioFemalebtn.setChecked(true);
                }
                else{
                    radioMalebtn.setChecked(true);
                }
            }else{
                utility.makeToast(BasicHealthEditActivity.this, "currentClient.sex.isEmpty" );
            }

        }
        else {
            if (firebaseAuth.getUid()!=null){
                currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            }

        }
    }

}
