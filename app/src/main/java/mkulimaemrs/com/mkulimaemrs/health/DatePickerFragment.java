package mkulimaemrs.com.mkulimaemrs.health;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;


/**
 * Created by Allymahmoud on 11/19/17.
 */

public class DatePickerFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        long now = System.currentTimeMillis() - 1000;
        long sevenDaysFromNow = now+(1000*60*60*24*7);
        long sevenDaysAgo = now-(1000*60*60*24*7);
        long threeMonthsAgo = now-(1000*60*60*24*90);



        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (DatePickerDialog.OnDateSetListener)
                        getActivity(), year, month, day);

        c.add(Calendar.MONTH, -11); // subtract 11 months from now
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
//        c.add(Calendar.YEAR, +3); // add 4 years to min date to have 2 years after now
//        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());

        //datePickerDialog.getDatePicker().setMinDate(threeMonthsAgo);
        datePickerDialog.getDatePicker().setMaxDate(now);

        return  datePickerDialog;
//        return new DatePickerDialog(getActivity(),
//                (DatePickerDialog.OnDateSetListener)
//                        getActivity(), year, month, day);
    }
}
