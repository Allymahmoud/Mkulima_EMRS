package mkulimaemrs.com.mkulimaemrs.health;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;

public class BasicHealthActivity extends AppCompatActivity {
    private Utility utility;
    private static LayoutInflater inflater=null;
    private LinearLayout allergiesLL;


    private TextView medicalRecordCount;
    private TextView medicalRecordViewers;

    private TextView height;
    private  TextView weight;
    private TextView sex;
    private TextView name;

    private TextView bloodGroup;
    private  TextView BMI;


    private ImageView profileImage;

    private Client currentClient;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_health);
        allergiesLL = (LinearLayout) findViewById(R.id.allergiesLL);

        name = (TextView) findViewById(R.id.client_name);
        height = (TextView) findViewById(R.id.height);
        sex = (TextView) findViewById(R.id.sex);
        weight = (TextView) findViewById(R.id.weight);
        bloodGroup = (TextView) findViewById(R.id.blood_group);
        BMI = (TextView) findViewById(R.id.bmi);
        medicalRecordCount = (TextView) findViewById(R.id.medical_record_count);
        medicalRecordViewers = (TextView) findViewById(R.id.medical_record_viewers);


        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        utility = new Utility();
        utility.checkPermissions(this);

        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
        }


        inflater = ( LayoutInflater )this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        configUI();
    }

    public void configUI(){
        if (currentClient != null){
            name.setText(currentClient.name);
            sex.setText(currentClient.sex);

            if (currentClient.medicalRecord != null){
                height.setText(currentClient.medicalRecord.height + " centimeter");
                weight.setText(currentClient.medicalRecord.weight + " Kg");
                bloodGroup.setText(currentClient.medicalRecord.bloodType);
                BMI.setText(currentClient.medicalRecord.bmi + " BMI");

                String[] allergyArray = utility.splitString(currentClient.medicalRecord.allergies, ",");
                for(String allergy : allergyArray){
                    View allergyView;
                    allergyView = inflater.inflate(R.layout.allergy_display, null);
                    TextView allergyname = (TextView) allergyView.findViewById(R.id.allergyitemname);
                    allergyname.setText(allergy);
                    allergiesLL.addView(allergyView);
                }

            }

            if (currentClient.hospitalVisits != null){
                String noMedicalRecords = String.valueOf(currentClient.hospitalVisits.size());
                medicalRecordCount.setText(noMedicalRecords);
            }
            else{
                medicalRecordCount.setText("0");
            }
            if (currentClient.medicalViewers != null){
                medicalRecordViewers.setText(String.valueOf(currentClient.medicalViewers.size()));
            }else {
                medicalRecordViewers.setText("0");
            }
        }


    }

    public void editBasicHealthAction(View view) {
        utility.segueToNewActivity(BasicHealthActivity.this, BasicHealthEditActivity.class);
    }
}
