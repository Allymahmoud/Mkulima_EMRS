package mkulimaemrs.com.mkulimaemrs.health;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;

public class HospitalVisitActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private EditText hospitalName;
    private  EditText doctorName;
    private EditText symptoms;
    private EditText diagnosis;
    private EditText medication;
    private  EditText diagnosisResults;
    private  EditText date;

    private String selectedDate;
    private String Key;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private Client currentClient;
    private Utility utility;
    private ImageView profileImage;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private Uri ImageCaptureUri;
    private Uri SavedimageUri;
    Uri filePath;
    ProgressDialog progressDialog;
    private static final String PROFILE_IMAGE_URI_INSTANCE_KEY = "saved_image_uri";
    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_visit);


        hospitalName = (EditText) findViewById(R.id.hospital_name);
        doctorName = (EditText) findViewById(R.id.doctors_name);
        symptoms = (EditText) findViewById(R.id.symptoms);
        diagnosis = (EditText) findViewById(R.id.diagnosis);
        medication = (EditText) findViewById(R.id.medication);
        diagnosisResults = (EditText) findViewById(R.id.results);
        date = (EditText) findViewById(R.id.date);

        profileImage = (ImageView)findViewById(R.id.client_image);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        utility = new Utility();
        currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
    }

    public void addDateaction(View view) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "Date dialog");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar cal = new GregorianCalendar(year, month, day);
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        Log.d("Selected date", dateFormat.format(cal.getTime()));
        selectedDate =  dateFormat.format(cal.getTime());
        date.setText(selectedDate);
        currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());

    }
    /*
    public String timeStamp(){
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

        return dateFormat.format(calendar.getTime());
    }

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }*/

    public void changeImage_Action(View view) {
        Toast toast = Toast.makeText(HospitalVisitActivity.this, "You clicked on ImageView", Toast.LENGTH_LONG);
        toast.show();

        //create an intent and send to camera
        Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                /*if (takePic.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePic, REQUEST_IMAGE_CAPTURE);
                }*/

        ContentValues values = new ContentValues(1);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
        ImageCaptureUri = getContentResolver()
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        takePic.putExtra(MediaStore.EXTRA_OUTPUT, ImageCaptureUri);
        takePic.putExtra("return-data", true);

        try {
            // Start a camera capturing activity
            startActivityForResult(takePic, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }


    /*
    returns the result intent from a take photo intent and then calls thr cropping methods
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                if (ImageCaptureUri != null) {
                    Log.d("imageUri", "not null");
                    // Send image taken from camera for cropping
                    beginCrop(ImageCaptureUri);
                } else {
                    Toast.makeText(this, "imageURi is empty", Toast.LENGTH_SHORT).show();
                }
            }
            if (requestCode == Crop.REQUEST_CROP) {
                // Update image view after image crop
                handleCrop(resultCode, data);
            }
        }
    }

    /*
    helper functions to crop the image
     */
    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            profileImage.setImageURI(null);
            filePath = Crop.getOutput(result);
            profileImage.setImageURI(Crop.getOutput(result));
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*
    loadImage- loads a profile photo file name from internal storage, if no image exist it just has the default view
     */
    private void loadImage() {
        // Load profile photo from internal storage
        try {

            FileInputStream fis = openFileInput(PROFILE_IMAGE_URI_INSTANCE_KEY);
            Bitmap bmap = BitmapFactory.decodeStream(fis);
            profileImage.setImageBitmap(bmap);
            fis.close();
        } catch (IOException e) {
            if (SavedimageUri != null) {
                Toast.makeText(this, "image in internal storage", Toast.LENGTH_SHORT).show();
                profileImage.setImageURI(SavedimageUri);

            } else {
                Toast.makeText(this, "found image uri null", Toast.LENGTH_SHORT).show();
                // Default profile photo if no photo saved before.
                profileImage.findViewById(R.id.client_image);
            }
        }

    }

    public void saveImage_Action(final HospitalVisit hospitalVisit) {
        if (filePath != null){
            if (currentClient != null){
                progressDialog.setMessage("Saving image to user to the server");
                progressDialog.show();
                StorageReference childRef = storageRef.child("medical records").child(currentClient.id).child( Key+ "image.jpg");
                //uploading the image
                UploadTask uploadTask = childRef.putFile(filePath);
                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()){

                            storageRef.child("medical records").child(currentClient.id).child( Key+ "image.jpg").getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> task) {
                                    if (task.isSuccessful()){

                                        String downloadUrl = task.getResult().toString();
                                        hospitalVisit.setImageUrl(downloadUrl);
                                        mDatabase.child("clients").child(currentClient.id).child("lastHospitalVisit").setValue(hospitalVisit.date);
                                        mDatabase.child("clients").child(currentClient.id).child("hospitalVisits").child(Key).setValue(hospitalVisit);
                                        progressDialog.cancel();
                                        utility.alertView("Success", "successfuly save the image  and hospital vist info", HospitalVisitActivity.this);
                                        finish();

                                    }else{
                                        progressDialog.cancel();
                                        utility.alertView("Error!", "Failed to update the image", HospitalVisitActivity.this);
                                    }

                                }
                            });
                        }else{
                            progressDialog.cancel();
                            utility.alertView("Error!", "Failed to update the image", HospitalVisitActivity.this);


                        }

                    }
                });


            }

        }else{
            utility.alertView("Error!", "Please select an image", HospitalVisitActivity.this);


        }

    }

    public void saveHospitalVisitInfo(){
        String hospitalName_input = hospitalName.getText().toString().trim();
        String doctorName_input=  doctorName.getText().toString().trim();
        String symptoms_input = symptoms.getText().toString().trim();
        String diagnosis_input = diagnosis.getText().toString().trim();
        String  medication_input = medication.getText().toString().trim();
        String diagnosisResults_input = diagnosisResults.getText().toString().trim();

        if (TextUtils.isEmpty(medication_input) || TextUtils.isEmpty(diagnosisResults_input) || TextUtils.isEmpty(symptoms_input) || TextUtils.isEmpty(diagnosis_input) || TextUtils.isEmpty(hospitalName_input) || TextUtils.isEmpty(doctorName_input) || selectedDate == null ){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else{
            String id;
            if (currentClient.hospitalVisits != null){
                int no_hospitalVisit = currentClient.hospitalVisits.size()+1;
                id = "Hospital Visit " + String.valueOf(no_hospitalVisit);
                Key = id;
            }
            else{
                id = "Hospital Visit 1";
                Key = id;
            }

            HospitalVisit hospitalVisit = new HospitalVisit(id, selectedDate,hospitalName_input,doctorName_input,symptoms_input,diagnosis_input, medication_input,diagnosisResults_input);
            saveImage_Action(hospitalVisit);
        }

    }


    public void save_action(View view) {
        saveHospitalVisitInfo();
    }
}
