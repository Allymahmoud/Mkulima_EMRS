package mkulimaemrs.com.mkulimaemrs.profile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;

public class ProfileActivity extends AppCompatActivity {

    private TextView medicalRecordCount;
    private TextView medicalRecordViewers;

    private TextView email;
    private  TextView name;
    private TextView sex;
    private TextView phonenumber;

    private TextView houseNo;
    private TextView street;
    private TextView region;
    private TextView country;

    private TextView EmergencyContactName;
    private TextView EmergencyContactEmail;
    private TextView EmergencyContactRelationship;
    private TextView EmergencyContactPhone;
    private TextView EmergencyContactAddress;

    private TextView clientAddress;


    private ImageView profileImage;

    private Client currentClient;
    private Utility utility;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_2);

        utility = new Utility();
        email = (TextView) findViewById(R.id.client_email);
        sex = (TextView) findViewById(R.id.client_sex);
        name = (TextView) findViewById(R.id.client_name);
        phonenumber = (TextView) findViewById(R.id.client_phone);

        houseNo = (TextView) findViewById(R.id.houseNo);
        street = (TextView) findViewById(R.id.street);
        region = (TextView) findViewById(R.id.region);
        country = (TextView) findViewById(R.id.country);
        profileImage = (ImageView)findViewById(R.id.client_image);

        EmergencyContactName = (TextView) findViewById(R.id.emergency_contact_name);
        EmergencyContactEmail = (TextView) findViewById(R.id.emergency_contact_email);
        EmergencyContactPhone= (TextView)findViewById(R.id.emergency_contact_phone);
        EmergencyContactRelationship = (TextView)findViewById(R.id.emergency_contact_relationship);
        EmergencyContactAddress = (TextView)findViewById(R.id.emergency_contact_address);

        clientAddress = (TextView)findViewById(R.id.client_address);

        medicalRecordCount = (TextView) findViewById(R.id.medical_record_count);
        medicalRecordViewers = (TextView) findViewById(R.id.medical_record_viewers);


        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        utility = new Utility();
        utility.checkPermissions(this);

        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
        }
        // render UI
        configUI();
    }

    public void editProfileAction(View view) {
        utility.segueToNewActivity(ProfileActivity.this, ProfileEditActivity.class);
    }


    private void configUI(){
        if (currentClient != null){



            name.setText(currentClient.name);
            if (currentClient.address != null){
                houseNo.setText(currentClient.address.houseNumber);
                street.setText(currentClient.address.street);
                region.setText(currentClient.address.region);
                country.setText(currentClient.address.country);

                clientAddress.setText(currentClient.address.region + ", " + currentClient.address.country);
            }
            email.setText(currentClient.email);
            phonenumber.setText(currentClient.phonenumber);
            sex.setText(currentClient.sex);


            if (currentClient.emergencyContacts != null){
                EmergencyContactName.setText(currentClient.emergencyContacts.get("Emergency Contact 1").name);
                EmergencyContactEmail.setText(currentClient.emergencyContacts.get("Emergency Contact 1").email);
                EmergencyContactPhone.setText(currentClient.emergencyContacts.get("Emergency Contact 1").phonenumber);
                EmergencyContactRelationship.setText(currentClient.emergencyContacts.get("Emergency Contact 1").relationship);
                EmergencyContactAddress.setText(currentClient.emergencyContacts.get("Emergency Contact 1").address);

            }
            if (currentClient.hospitalVisits != null){
                String noMedicalRecords = String.valueOf(currentClient.hospitalVisits.size());
                medicalRecordCount.setText(noMedicalRecords);
            }
            else{
                medicalRecordCount.setText("0");
            }
            if (currentClient.medicalViewers != null){
                medicalRecordViewers.setText(String.valueOf(currentClient.medicalViewers.size()));
            }else {
                medicalRecordViewers.setText("0");
            }

        }
        else {
            if (firebaseAuth.getUid()!=null){
                currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            }

        }
    }
}
