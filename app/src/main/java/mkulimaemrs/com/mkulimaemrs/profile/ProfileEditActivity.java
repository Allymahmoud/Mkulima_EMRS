package mkulimaemrs.com.mkulimaemrs.profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Address;
import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.EmergencyContact;

public class ProfileEditActivity extends AppCompatActivity {

    private TextView name;
    private TextView medicalRecordCount;
    private TextView medicalRecordViewers;

    private EditText email;
    private EditText phonenumber;
    private RadioGroup radioSexGroup;
    private RadioButton radioMalebtn;
    private RadioButton radioFemalebtn;

    private EditText EmergencyContactName;
    private EditText EmergencyContactEmail;
    private EditText EmergencyContactRelationship;
    private EditText EmergencyContactPhone;
    private EditText EmergencyContactAddress;

    private EditText houseNo;
    private EditText street;
    private EditText region;
    private EditText country;

    private Client currentClient;
    private Utility utility;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    private ProgressDialog progressDialog;

    private boolean checked;
    private String sex;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        name = (TextView) findViewById(R.id.client_name);
        medicalRecordCount = (TextView) findViewById(R.id.medical_record_count);
        medicalRecordViewers = (TextView) findViewById(R.id.medical_record_viewers);


        email = (EditText)findViewById(R.id.client_email);
        phonenumber = (EditText)findViewById(R.id.client_phone);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        radioMalebtn = (RadioButton)findViewById(R.id.male);
        radioFemalebtn = (RadioButton)findViewById(R.id.female);

        EmergencyContactName = (EditText)findViewById(R.id.emergency_contact_name);
        EmergencyContactEmail = (EditText)findViewById(R.id.emergency_contact_email);
        EmergencyContactPhone= (EditText)findViewById(R.id.emergency_contact_phone);
        EmergencyContactRelationship = (EditText)findViewById(R.id.emergency_contact_relationship);
        EmergencyContactAddress = (EditText)findViewById(R.id.emergency_contact_address);

        houseNo = (EditText)findViewById(R.id.houseNo);
        street = (EditText)findViewById(R.id.street);
        region = (EditText)findViewById(R.id.region);
        country = (EditText)findViewById(R.id.country);



        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        progressDialog = new ProgressDialog(this);

        utility = new Utility();
        utility.checkPermissions(this);
        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            sex = currentClient.sex;
        }
        configUI();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void saveAction(View view) {
        // TODO: save the info to firebase
        //finish();
        saveEmergencycontact();
    }

    private boolean checkifAllTextfieldsareFilled(){

        String input_houseNo = houseNo.getText().toString().trim();
        String input_street = street.getText().toString().trim();
        String input_Region = region.getText().toString().trim();
        String input_Country = country.getText().toString().trim();

        String input_EmergencyContactName = EmergencyContactName.getText().toString().trim();
        String input_EmergencyContactEmail = EmergencyContactEmail.getText().toString().trim();
        String input_EmergencyContactPhone = EmergencyContactPhone.getText().toString().trim();
        String input_EmergencyContactRelationship = EmergencyContactRelationship.getText().toString().trim();
        String input_EmergencyContactAddress = EmergencyContactAddress.getText().toString().trim();

        String input_email = email.getText().toString().trim();
        String input_phone = phonenumber.getText().toString().trim();
        String input_sex = sex;


        if (TextUtils.isEmpty(input_EmergencyContactName) || TextUtils.isEmpty(input_EmergencyContactEmail) || TextUtils.isEmpty(input_EmergencyContactPhone) || TextUtils.isEmpty(input_EmergencyContactRelationship) ||
                TextUtils.isEmpty(input_email) || TextUtils.isEmpty(input_phone) || input_sex == null || TextUtils.isEmpty(input_houseNo) || TextUtils.isEmpty(input_street) || TextUtils.isEmpty(input_Region) || TextUtils.isEmpty(input_Country)){
            return false;
        }
        return true;


    }

    private void saveAddress(){
        String input_houseNo = houseNo.getText().toString().trim();
        String input_street = street.getText().toString().trim();
        String input_Region = region.getText().toString().trim();
        String input_Country = country.getText().toString().trim();

        if (TextUtils.isEmpty(input_houseNo) || TextUtils.isEmpty(input_street) || TextUtils.isEmpty(input_Region) || TextUtils.isEmpty(input_Country)){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else {
            Address newAddress = new Address(input_houseNo, input_street, input_Region, input_Country);
            final FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();

            if (mFirebaseUser != null){
                progressDialog.show();
                String uid = mFirebaseUser.getUid();
                mDatabase.child("clients").child(uid).child("address").setValue(newAddress)
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialog.cancel();
                                if (task.isSuccessful()){
                                    finish();
                                    utility.getUserInfo(mDatabase,firebaseAuth.getUid());


                                }else {
                                    Log.d("task failure",task.getException().getMessage());
                                    utility.alertView("Error!, Could Not save address", task.getException().getLocalizedMessage(),ProfileEditActivity.this);

                                }

                            }
                        });



            }else{
                utility.alertView("Could Not Save changes","Something went wrong", this);
            }

        }

    }

    private void saveEmergencycontact(){
        String input_EmergencyContactName = EmergencyContactName.getText().toString().trim();
        String input_EmergencyContactEmail = EmergencyContactEmail.getText().toString().trim();
        String input_EmergencyContactPhone = EmergencyContactPhone.getText().toString().trim();
        String input_EmergencyContactRelationship = EmergencyContactRelationship.getText().toString().trim();
        String input_EmergencyContactAddress = EmergencyContactAddress.getText().toString().trim();

        if (!checkifAllTextfieldsareFilled()){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else {

            EmergencyContact emergencyContact = new EmergencyContact(input_EmergencyContactName,input_EmergencyContactEmail, input_EmergencyContactPhone, input_EmergencyContactRelationship, input_EmergencyContactAddress);
            final FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();

            if (mFirebaseUser != null){
                progressDialog.show();
                String uid = mFirebaseUser.getUid();
                mDatabase.child("clients").child(uid).child("emergencyContacts").child("Emergency Contact 1").setValue(emergencyContact)
                        .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialog.cancel();
                                if (task.isSuccessful()){
                                    saveUserInfo();
                                    saveAddress();


                                }else {
                                    Log.d("task failure",task.getException().getMessage());
                                    utility.alertView("Error!, Could Not save emegency contact", task.getException().getLocalizedMessage(),ProfileEditActivity.this);

                                }

                            }
                        });



            }else{
                utility.alertView("Could Not Save changes","failed to retrieve user", this);
            }

        }


    }

    private void saveUserInfo(){
        String input_email = email.getText().toString().trim();
        String input_phone = phonenumber.getText().toString().trim();
        String input_sex = sex;




        if (TextUtils.isEmpty(input_email) || TextUtils.isEmpty(input_phone) || input_sex == null ){
            utility.alertView("Could not proceed", "no field can be empty", this);
        }
        else {

            final FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();

            if (mFirebaseUser != null){
                String uid = mFirebaseUser.getUid();
                mDatabase.child("clients").child(uid).child("phonenumber").setValue(input_phone);
                mDatabase.child("clients").child(uid).child("sex").setValue(input_sex);

            }else{
                utility.alertView("Could Not Save changes","Something went wrong", this);
            }

        }

    }



    private void configUI(){
        if (currentClient != null){
            name.setText(currentClient.name);

            if (currentClient.address != null){
                houseNo.setText(currentClient.address.houseNumber);
                street.setText(currentClient.address.street);
                region.setText(currentClient.address.region);
                country.setText(currentClient.address.country);
            }
            email.setText(currentClient.email);
            phonenumber.setText(currentClient.phonenumber);

            // configure sex
            if (!currentClient.sex.isEmpty()){
                if (currentClient.sex.contentEquals("Female")){
                    radioFemalebtn.setChecked(true);
                }
                else{
                    radioMalebtn.setChecked(true);
                }
            }else{
                utility.makeToast(ProfileEditActivity.this, "currentClient.sex.isEmpty" );
            }

            if (currentClient.emergencyContacts != null){
                EmergencyContactName.setText(currentClient.emergencyContacts.get("Emergency Contact 1").name);
                EmergencyContactEmail.setText(currentClient.emergencyContacts.get("Emergency Contact 1").email);
                EmergencyContactPhone.setText(currentClient.emergencyContacts.get("Emergency Contact 1").phonenumber);
                EmergencyContactRelationship.setText(currentClient.emergencyContacts.get("Emergency Contact 1").relationship);
                EmergencyContactAddress.setText(currentClient.emergencyContacts.get("Emergency Contact 1").address);

            }
            if (currentClient.hospitalVisits != null){
                String noMedicalRecords = String.valueOf(currentClient.hospitalVisits.size());
                medicalRecordCount.setText(noMedicalRecords);
            }
            else{
                medicalRecordCount.setText("0");
            }
            if (currentClient.medicalViewers != null){
                medicalRecordViewers.setText(String.valueOf(currentClient.medicalViewers.size()));
            }else {
                medicalRecordViewers.setText("0");
            }

        }
        else {
            if (firebaseAuth.getUid()!=null){
                currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            }

        }
    }

    public void sexSelected(View view) {
        // Is the button now checked?
        checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.male:
                if (checked)
                    // Pirates are the best
                    sex = "Male";
                    utility.makeToast(this,"male");
                    break;
            case R.id.female:
                if (checked)
                    // Ninjas rule
                    sex = "Female";
                    utility.makeToast(this,"female");
                    break;
        }
    }
}

