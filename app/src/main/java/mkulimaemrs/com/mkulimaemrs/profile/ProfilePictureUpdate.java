package mkulimaemrs.com.mkulimaemrs.profile;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import mkulimaemrs.com.mkulimaemrs.R;
import mkulimaemrs.com.mkulimaemrs.Utility;
import mkulimaemrs.com.mkulimaemrs.classes.Client;

public class ProfilePictureUpdate extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private Client currentClient;
    private Utility utility;
    private ImageView profileImage;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    private Uri ImageCaptureUri;
    private Uri SavedimageUri;
    Uri filePath;
    ProgressDialog progressDialog;
    private static final String PROFILE_IMAGE_URI_INSTANCE_KEY = "saved_image_uri";
    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_picture_update);
        profileImage = (ImageView)findViewById(R.id.client_image);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        utility = new Utility();
        currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
    }
}
