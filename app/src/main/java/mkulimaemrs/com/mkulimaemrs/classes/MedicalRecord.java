package mkulimaemrs.com.mkulimaemrs.classes;

/**
 * Created by Allymahmoud on 5/21/18.
 */
public class MedicalRecord{
    public String height;
    public String weight;
    public String bmi;

    public String bloodType;
    public String allergies;

    public MedicalRecord(){}

    public MedicalRecord(String height, String weight, String bmi, String bloodType, String allergies){
        this.height = height;
        this.weight = weight;
        this.bmi = bmi;
        this.bloodType = bloodType;
        this.allergies = allergies;

    }

}
