package mkulimaemrs.com.mkulimaemrs.classes;

/**
 * Created by Allymahmoud on 5/21/18.
 */

public class HospitalVisit{
    public String id;
    public String date;
    public String hospitalName;
    public String doctorName;
    public String symptoms;
    public String diagnosis;
    public String medication;
    public String results;
    public String imageUrl;
    // public HashMap<String,String> imageUrls;
    // public HashMap<String,String> medication;

    public HospitalVisit(){}

    public HospitalVisit(String id, String date, String hospitalName, String doctorName, String symptoms, String diagnosis, String medication, String results){
        this.id = id;
        this.date = date;
        this.hospitalName = hospitalName;
        this.doctorName = doctorName;
        this.symptoms = symptoms;
        this.diagnosis = diagnosis;
        this.medication = medication;
        this.results = results;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}