package mkulimaemrs.com.mkulimaemrs.classes;

/**
 * Created by Allymahmoud on 5/21/18.
 */

public class EmergencyContact{
    public String relationship;
    public String name;
    public String phonenumber;
    public String email;
    public String address;

    public EmergencyContact(){}

    public EmergencyContact(String name, String email, String phonenumber, String relationship,   String address){
        this.relationship = relationship;
        this.name = name;
        this.phonenumber = phonenumber;
        this.email = email;

        this.address = address;
    }

}