package mkulimaemrs.com.mkulimaemrs.classes;

/**
 * Created by Allymahmoud on 5/21/18.
 */
public class Address{
    public String houseNumber;
    public String street;
    public String region;
    public String country;

    public Address(){}

    public Address(String houseN, String strt, String region, String country){
        this.houseNumber = houseN;
        this.street = strt;
        this.region = region;
        this.country = country;
    }

}
