package mkulimaemrs.com.mkulimaemrs.classes;

import java.util.HashMap;

/**
 * Created by Allymahmoud on 5/21/18.
 */

public class Client
{

    //core variables
    public String name;
    public String phonenumber;
    public String email;
    public String password;
    public String id;


    public String role;
    public String sex;
    public HashMap<String,EmergencyContact> emergencyContacts;


    public String photoURL;

    //Address
    public Address address;

    //health
    public MedicalRecord medicalRecord;
    public HashMap<String,HospitalVisit> hospitalVisits;
    public HashMap<String,String> medicalViewers;



    //timing information
    public String dateJoined;
    public String lastHospitalVisit;

    public Client(){

    }

    public Client(String name, String email, String password, String role, String id){
        this.name = name;
        this.email = email;
        this.phonenumber = "";
        this.password = password;
        this.id = id;

        this.role = role;

        this.sex = "";



        //timing information
        this.dateJoined="";
        this.lastHospitalVisit="";


        this.photoURL = "";

        this.emergencyContacts = new HashMap<>();
        this.emergencyContacts.put("Emergency Contact 1", new EmergencyContact("Ally Mahmoud", "Father", "+255656346557", "ally@moud.tz", "Dodoma, Tanzania") );
        this.medicalViewers = new HashMap<>();
        this.medicalViewers.put("medical viewer 1","Tumaini Hospital");

        //Address
        //this.address;

        //health
        //this.medicalRecord;
        //this.hospitalVisits;
    }



    //setter methods
    public void setid(String Id){
        this.id = Id;
    }

}


