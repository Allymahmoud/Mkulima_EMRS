package mkulimaemrs.com.mkulimaemrs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mkulimaemrs.com.mkulimaemrs.classes.Client;
import mkulimaemrs.com.mkulimaemrs.classes.HospitalVisit;
import mkulimaemrs.com.mkulimaemrs.health.BasicHealthActivity;
import mkulimaemrs.com.mkulimaemrs.health.HospitalVisitActivity;
import mkulimaemrs.com.mkulimaemrs.health.MedicalRecordsActivity;
import mkulimaemrs.com.mkulimaemrs.profile.ProfileActivity;

public class MainActivity extends AppCompatActivity {
    private Utility utility;
    private TextView name;


    private ImageView profileImage;

    private Client currentClient;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    //creating reference to firebase storage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://mkulima-emrs.appspot.com/");    //change the url according to your firebase app


    BarChart chart ;
    HashMap<String,Integer> monthlycount;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels ;
    BarDataSet Bardataset ;
    BarData BARDATA ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.client_name);


        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();


        utility = new Utility();
        utility.checkPermissions(this);

        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            getUserinfoDatabase(firebaseAuth.getUid());
        }

        chart = (BarChart) findViewById(R.id.chart1);


    }

    @Override
    protected void onResume() {
        super.onResume();
        monthlycount = null;
        if (firebaseAuth.getUid()!=null){
            currentClient = utility.getUserInfo(mDatabase,firebaseAuth.getUid());
            getUserinfoDatabase(firebaseAuth.getUid());
        }


    }

    public void getUserinfoDatabase(String userId){
        Query userInfoQuery = mDatabase.child("clients").child(userId);
        userInfoQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentClient = dataSnapshot.getValue(Client.class);
                if (currentClient != null){
                    configUI();

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void configUI(){
        if (currentClient != null){
            name.setText(currentClient.name);

            if (currentClient.hospitalVisits != null){
                for (HospitalVisit hospitalVisit: currentClient.hospitalVisits.values()){
                    if (monthlycount != null) {

                        String month = hospitalVisit.date.split(" ")[0];
                        Log.d("month",month);
                        if (monthlycount.containsKey(month)) {
                            monthlycount.put(month, monthlycount.get(month)+1);
                        } else {
                            monthlycount.put(month, 1);
                        }

                    }
                    else{
                        monthlycount = new HashMap<String, Integer>();
                        String month = hospitalVisit.date.split(" ")[0];
                        monthlycount.put(month, 1);
                    }

                }

                populateBardata();



                Bardataset = new BarDataSet(BARENTRY, "Hospital Visits");

                BARDATA = new BarData(BarEntryLabels, Bardataset);

                Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);

                chart.setData(BARDATA);

                chart.animateY(3000);


            }
        }


    }

    public void populateBardata(){
        BARENTRY = new ArrayList<>();
        BarEntryLabels = new ArrayList<String>();

        int i = 0;

        for ( Map.Entry<String, Integer> entry : monthlycount.entrySet() ) {
            String key = entry.getKey();
            Integer value = entry.getValue();

            BARENTRY.add(new BarEntry(value, i));
            BarEntryLabels.add(key);
            i++;
        }
    }

    public void AddValuesToBARENTRY(){

        BARENTRY.add(new BarEntry(2f, 0));
        BARENTRY.add(new BarEntry(4f, 1));
        BARENTRY.add(new BarEntry(6f, 2));
        BARENTRY.add(new BarEntry(8f, 3));
        BARENTRY.add(new BarEntry(7f, 4));
        BARENTRY.add(new BarEntry(3f, 5));

    }

    public void AddValuesToBarEntryLabels(){

        BarEntryLabels.add("January");
        BarEntryLabels.add("February");
        BarEntryLabels.add("March");
        BarEntryLabels.add("April");
        BarEntryLabels.add("May");
        BarEntryLabels.add("June");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        MenuItem homeItem = mBottomNav.getMenu().getItem(0);
//        if (mSelectedItem != homeItem.getItemId()) {
//            // select home item
//            selectFragment(homeItem);
//            // uncheck the other items.
//            MenuItem menuItem = mBottomNav.getMenu().getItem(0);
//            menuItem.setChecked(menuItem.getItemId() == homeItem.getItemId());
//
//        } else {
//            super.onBackPressed();
//        }
    }

    public void profileCardAction(View view) {
        utility.segueToNewActivity(MainActivity.this, ProfileActivity.class);

    }

    public void basicHealthCardAction(View view) {
        utility.segueToNewActivity(MainActivity.this, BasicHealthActivity.class);
    }

    public void hospitalVisitAction(View view) {
        utility.segueToNewActivity(MainActivity.this, HospitalVisitActivity.class);
    }

    public void medicalRecordAction(View view) {
        utility.segueToNewActivity(MainActivity.this, MedicalRecordsActivity.class);
    }
}
